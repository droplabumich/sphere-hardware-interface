
#include <string>
#include <stdlib.h>
#include <iomanip>      // std::setprecision
#include <stdio.h>
#include <string>
#include <time.h>
#include <iostream>
#include <fstream>

#include "ros/ros.h"
#include <ros/console.h>
#include <diagnostic_updater/diagnostic_updater.h>
#include <diagnostic_updater/publisher.h>

#include "CommandInterface.h"
#include <tf/transform_datatypes.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>

#include "sensor_msgs/Imu.h"
#include "serial/serial.h"
#include "std_msgs/Float32.h"
#include "std_msgs/String.h"
#include "sensor_msgs/NavSatFix.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "geometry_msgs/TransformStamped.h"
#include <utils/LowLevelData.h>
#include <utils/ThrustersData.h>
#include <utils/PressureSensor.h>
#include "utils/NED.h"
#include "utils/StampedString.h"
#include "utils/StampedBool.h"

#include "utils/Altitude.h"
#include <sp_adapter/setUI.h>
#include <std_srvs/Empty.h>


#define MAX_DATE 36

using namespace std;

template <typename T>
static inline void load_parameter(std::string param, T& var)
{
	if(!ros::param::get(param, var)) {
		ROS_FATAL("Invalid parameters for %s in param server!", param.c_str());
		ros::shutdown();
	}
}

std::string get_date(void)
{
   time_t now;
   char the_date[MAX_DATE];

   the_date[0] = '\0';

   now = time(NULL);

   if (now != -1)
   {
      strftime(the_date, MAX_DATE, "%d_%m_%Y_%h_%s", gmtime(&now));
   }

   return std::string(the_date);
}

class SphereAdapter {
public:

    SENSOR_DATA::State state;
    SerialProtocol protocol;

    ros::NodeHandle nh; //, private_nh("~");
    std::string veh_name;						

    // ROS Subscribers
    ros::Subscriber sub;
    ros::Subscriber enable_camera_sub;

    // ROS Publishers
    ros::Publisher ll_data_pub;
    ros::Publisher ll_string_pub;
    ros::Publisher depth_pub;
    ros::Publisher imu_pub;
    ros::Publisher gps_pub;
    ros::Publisher altimeter_pub;

    // ROS Services
    ros::ServiceServer ui_serv;
	ros::ServiceServer press_serv;
    ros::ServiceServer enableStrobe;

    serial::Serial ser;

    // ROS diagnostics
    diagnostic_updater::Updater imu_updater, gps_updater, va500P_updater;

    std::shared_ptr<diagnostic_updater::HeaderlessTopicDiagnostic> imu_freq, gps_freq, altimeter_freq,
                                                                    pressure_freq;
    ros::Timer timer;
    std::string name, port;
	int32_t baud, atmosphericPressure;

	tf2_ros::StaticTransformBroadcaster sensor_tf_broadcaster;

    double imu_min_freq, imu_max_freq, gps_min_freq, gps_max_freq, altimeter_min_freq, altimeter_max_freq,
            pressure_min_freq, pressure_max_freq;

    bool enable_cam_trig; 

    SphereAdapter(ros::NodeHandle& nh_, std::string veh_name_): protocol(&state), nh(nh_), veh_name(veh_name_) {

        // Load parameters from private node handle.
        name =  ros::this_node::getName();
        getConf();

        // Create subscribers and publishers
        sub = nh.subscribe("thruster_data", 10, &SphereAdapter::thruster_callback, this);
        enable_camera_sub = nh.subscribe("enable_camera_trigger", 10, &SphereAdapter::enable_camera_trigger, this);
        ll_data_pub = nh.advertise<utils::LowLevelData>("ll_data", 1000);
        ll_string_pub = nh.advertise<utils::StampedString>("ll_raw_string", 1000);

        depth_pub = nh.advertise<utils::PressureSensor>("/"+veh_name+"/nav_sensors/pressure_sensor", 1000);
        imu_pub = nh.advertise<sensor_msgs::Imu>("/"+veh_name+"/nav_sensors/imu", 1000);
        gps_pub = nh.advertise<sensor_msgs::NavSatFix>("/"+veh_name+"/nav_sensors/gps", 1000);
        altimeter_pub = nh.advertise<utils::Altitude>("/"+veh_name+"/nav_sensors/altitude", 1000);

        // Create Services
        ui_serv = nh.advertiseService("setUI",  &SphereAdapter::setUI_callback, this);
        press_serv = nh.advertiseService("resetPressure",  &SphereAdapter::set_atmosphericPressure_callback, this);


        // Set Hardware diagnostics
        //imu_min_freq = 9; // If you update these values, the
        //imu_max_freq = 14; // HeaderlessTopicDiagnostic will use the new values.
        imu_freq = std::make_shared<diagnostic_updater::HeaderlessTopicDiagnostic>("/"+veh_name+"/nav_sensors/imu", imu_updater, diagnostic_updater::FrequencyStatusParam(&imu_min_freq, &imu_max_freq, 0.1, 10));
        gps_freq = std::make_shared<diagnostic_updater::HeaderlessTopicDiagnostic>("/"+veh_name+"/nav_sensors/gps", gps_updater, diagnostic_updater::FrequencyStatusParam(&gps_min_freq, &gps_max_freq, 0.1, 10));
        altimeter_freq = std::make_shared<diagnostic_updater::HeaderlessTopicDiagnostic>("/"+veh_name+"/nav_sensors/pressure_sensor", va500P_updater, diagnostic_updater::FrequencyStatusParam(&altimeter_min_freq, &altimeter_max_freq, 0.1, 10));
        pressure_freq = std::make_shared<diagnostic_updater::HeaderlessTopicDiagnostic>("/"+veh_name+"/nav_sensors/altitude", va500P_updater, diagnostic_updater::FrequencyStatusParam(&pressure_min_freq, &pressure_max_freq, 0.1, 10));


        imu_updater.setHardwareID("XSens IMU");
        gps_updater.setHardwareID("EM506 GPS");
        va500P_updater.setHardwareID("VA500 RAP");
        timer = nh.createTimer(ros::Duration(0.1), &SphereAdapter::updateDiagnostics, this);

        setupTransforms();

        ser.setPort(port);
        ser.setBaudrate(baud);
        serial::Timeout to = serial::Timeout(50, 50, 0, 50, 0);
        ser.setTimeout(to);

      atmosphericPressure = 100000;

        // Attach callbacks for the different received messages
        protocol.attach(SerialProtocol::ST, this, &SphereAdapter::onST);
        protocol.attach(SerialProtocol::RT, this, &SphereAdapter::onRT);
        protocol.attach(SerialProtocol::RD, this, &SphereAdapter::onRD);
        protocol.attach(SerialProtocol::RA, this, &SphereAdapter::onRA);
        protocol.attach(SerialProtocol::AK, this, &SphereAdapter::onAK);
        protocol.attach(SerialProtocol::DM, this, &SphereAdapter::onDM);
        protocol.attach(SerialProtocol::AD, this, &SphereAdapter::onAD);
        protocol.attach(SerialProtocol::SS, this, &SphereAdapter::onSS);
        protocol.attach(SerialProtocol::UI, this, &SphereAdapter::onUI);
        protocol.attach(SerialProtocol::RL, this, &SphereAdapter::onRL);

        enable_cam_trig = false;
        ROS_INFO("Initialized node %s",name.c_str());


    }

    void enable_camera_trigger(const utils::StampedBool::ConstPtr &msg) {
        enable_cam_trig = msg->enable;
        int id = DATA_TYPES::cameraTriggerEnable;
        float data;
        if (enable_cam_trig) {
            data = 1;   
        } else {
            data = 0;
        }
        
        int length;
        std::string message(protocol.createmsg(SerialProtocol::DM, id, data, &length ));
        ser.write(message);
        ROS_DEBUG("Setting trigger enable/disable data");

    }

    void getConf() {
        bool success;
        nh.param<std::string>(name+"/port", port, "/dev/ttyACM0");
        nh.param<int32_t>(name+"/baud", baud, 115200);

        load_parameter(veh_name+"/altimeter/diagnostics/min_freq", altimeter_min_freq);
        load_parameter(veh_name+"/altimeter/diagnostics/max_freq", altimeter_max_freq);
        load_parameter(veh_name+"/imu/diagnostics/min_freq", imu_min_freq);
        load_parameter(veh_name+"/imu/diagnostics/max_freq", imu_max_freq);
        load_parameter(veh_name+"/gps/diagnostics/min_freq", gps_min_freq);
        load_parameter(veh_name+"/gps/diagnostics/max_freq", gps_max_freq);
        load_parameter(veh_name+"/pressure/diagnostics/min_freq", pressure_min_freq);
        load_parameter(veh_name+"/pressure/diagnostics/max_freq", pressure_max_freq);


    }

  void updateDiagnostics(const ros::TimerEvent&) {
    va500P_updater.update();
    gps_updater.update();
    imu_updater.update();

  }

	void setupTransforms() {
        std::vector<geometry_msgs::TransformStamped> transforms;

    	geometry_msgs::TransformStamped gps_transform;
    	gps_transform.header.stamp = ros::Time::now();
    	gps_transform.header.frame_id = veh_name+"_ned";
    	gps_transform.child_frame_id = veh_name+"/gps_frame";
    	std::vector<double>  tf_gps;
    	load_parameter("/" + veh_name + "/gps/tf", tf_gps);
    	gps_transform.transform.translation.x = tf_gps[0];
    	gps_transform.transform.translation.y = tf_gps[1];
    	gps_transform.transform.translation.z = tf_gps[2];
    	tf2::Quaternion quat; 
    	quat.setRPY(degree2radian(tf_gps[3]),degree2radian(tf_gps[4]),degree2radian(tf_gps[5]));
    	gps_transform.transform.rotation.x = quat.x();
    	gps_transform.transform.rotation.y = quat.y();
    	gps_transform.transform.rotation.z = quat.z();
    	gps_transform.transform.rotation.w = quat.w();
        transforms.push_back(gps_transform);

        geometry_msgs::TransformStamped imu_transform;
        imu_transform.header.stamp = ros::Time::now();
        imu_transform.header.frame_id = veh_name+"_ned";
        imu_transform.child_frame_id = veh_name+"/imu_frame";
        std::vector<double>  tf_imu;
        load_parameter("/" + veh_name + "/imu/tf", tf_imu);
        imu_transform.transform.translation.x = tf_imu[0];
        imu_transform.transform.translation.y = tf_imu[1];
        imu_transform.transform.translation.z = tf_imu[2];
        tf2::Quaternion quat_imu;
        quat_imu.setRPY(degree2radian(tf_imu[3]),degree2radian(tf_imu[4]),degree2radian(tf_imu[5]));
        imu_transform.transform.rotation.x = quat_imu.x();
        imu_transform.transform.rotation.y = quat_imu.y();
        imu_transform.transform.rotation.z = quat_imu.z();
        imu_transform.transform.rotation.w = quat_imu.w();
        transforms.push_back(imu_transform);


        geometry_msgs::TransformStamped externalPressure_transform;
        externalPressure_transform.header.stamp = ros::Time::now();
        externalPressure_transform.header.frame_id = veh_name+"_ned";
        externalPressure_transform.child_frame_id = veh_name+"/externalPressure_frame";
        std::vector<double>  tf_externalPressure;
        load_parameter("/" + veh_name + "/pressure/tf", tf_externalPressure);
        externalPressure_transform.transform.translation.x = tf_externalPressure[0];
        externalPressure_transform.transform.translation.y = tf_externalPressure[1];
        externalPressure_transform.transform.translation.z = tf_externalPressure[2];
        tf2::Quaternion quat_externalPressure;
        quat_externalPressure.setRPY(degree2radian(tf_externalPressure[3]),
                                              degree2radian(tf_externalPressure[4]),
                                              degree2radian(tf_externalPressure[5]));
        externalPressure_transform.transform.rotation.x = quat_externalPressure.x();
        externalPressure_transform.transform.rotation.y = quat_externalPressure.y();
        externalPressure_transform.transform.rotation.z = quat_externalPressure.z();
        externalPressure_transform.transform.rotation.w = quat_externalPressure.w();
        transforms.push_back(externalPressure_transform);

        sensor_tf_broadcaster.sendTransform(transforms);
	}

  double degree2radian(double degrees) {
    return (degrees * M_PI) / 180 ;
  }

  double radian2degree(double radians) {
    return (radians * 180) / M_PI ;
  }


  bool setUI_callback(sp_adapter::setUI::Request& req, sp_adapter::setUI::Response& res) {
        if (ser.isOpen()) {
            ROS_INFO_STREAM("Sending UI command"<<req.ui_mode);
            int message_length;
            std::string message(protocol.createmsg(SerialProtocol::UI ,req.ui_mode, &message_length));
            ser.write(message);
            res.success = true;
        }
        else {
            res.success = false;
            }
        return true;
    }

  bool set_atmosphericPressure_callback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res) {
		atmosphericPressure = state.sensors[DATA_TYPES::press_ext].value;	

  } 

  void connect() {
    uint8_t buffer[2];

        bool first_failure = true;

        std::string filename = "hw_iface_log";
        std::string path;
        if (nh.getParam("logpath", path)) {
             std::cout << "Logging Hardware Adapter to to path: "<<path <<std::endl;
        }
//        std::string path="/home/odroid/odroid-development/catkin_ws/src/sp_adapter/logs";

        std::string pathFilename = path + "/" + filename + ".txt";

        ofstream file_log;
        file_log.open(pathFilename);



        while (ros::ok())
        {
	    
            try
            {
                ser.open();
            }
            catch(const serial::IOException& e)
            {
                ROS_DEBUG("Unable to connect to port.");
            }
            if (ser.isOpen())
            {
                ROS_INFO("Successfully connected to serial port.");
                first_failure = true;
                usleep(10000); // Wait for 10 ms 
                synchronize_time();
                try
                {
                    while (ros::ok()) {
//			            if (file_log.is_open())
//                        {
//                            std::cout << "File successfully open";
//                        }
//                        else
//                        {
//                            std::cout << "Error opening file";
//                        }
                        while (ser.available() > 0) {
                            ser.read(buffer, 1);
                            protocol.process_char(buffer[0]);
                            file_log <<buffer[0];
                            std::cout << buffer[0];
                        }

                        if (protocol.newMessagereceived() ) {
			                char* lastmessage = protocol.lastMessage();
                            utils::StampedString msg;
                            msg.header.stamp = ros::Time::now();
                            msg.data_string = lastmessage;
                            protocol.parse(lastmessage);
                            ll_string_pub.publish(msg);
                        }

                        ros::spinOnce();
                        usleep(1000);
                    }
                }
                catch(const std::exception& e)
                {
                    if (ser.isOpen()) ser.close();
                    ROS_ERROR_STREAM(e.what());
                    ROS_INFO("Attempting reconnection after error.");
                    ros::Duration(1.0).sleep();
                }
            }
            else
            {
                ROS_WARN_STREAM_COND(first_failure, "Could not connect to serial device "
                        << port << ". Trying again every 1 second.");
                first_failure = false;
                ros::Duration(1.0).sleep();
            }
        }
        file_log.close();
    }

    uint32_t onST(uint32_t myarg) {

        printf("Got St! \n\r");
        //printf("UL: %8.4f \n\r", state.sensors[DATA_TYPES::pwm_ul].value);
        //printf("UR: %8.4f \n\r", state.sensors[DATA_TYPES::pwm_ur].value);
        //printf("FL: %8.4f \n\r", state.sensors[DATA_TYPES::pwm_fl].value);
        //printf("FR: %8.4f \n\r", state.sensors[DATA_TYPES::pwm_fr].value);

        return 0;
    }

    uint32_t onRL(uint32_t myarg) {
        printf("Got RL for target %i \n\r", protocol.redirectId);

        return 0;
    }

    uint32_t onRT(uint32_t myarg) {
        printf("Got RT \n\r");
        return 0;
    }

    uint32_t onRD(uint32_t myarg) {
        printf("Got RD \n\r");
        return 0;
    }

    uint32_t onRA(uint32_t myarg) {
        printf("Got RA \n\r");
        return 0;
    }

    uint32_t onAK(uint32_t myarg) {
        printf("Got AK for cmd %i  \n\r", (int) myarg);
        return 0;
    }

    uint32_t onDM(uint32_t myarg) {
        printf("Got DM \n\r");
        return 0;
    }

    uint32_t onAD(uint32_t myarg) {

        publish_lldata();

        for (int i=0; i<DATA_TYPES::NumberOfTypes;i++) {
            switch (protocol.lastMsgTypes[i]) {
                case DATA_TYPES::press_ext:
                    publish_depth();
                    break;
                case DATA_TYPES::roll:
                    publish_imu();
                    break;
                case DATA_TYPES::latitude:
                    publish_gps();
                    break;
                case DATA_TYPES::altitude:
                    publish_altitude();
                    break;
                default:
                    break;
            }
        }
  
        return 0;
    }

    void publish_lldata() {
        utils::LowLevelData msg;

        msg.header.stamp = ros::Time::now();
        msg.header.frame_id = "/world";
        msg.temp_int_hih = state.sensors[DATA_TYPES::temp_int_hih].value;
        msg.temp_int_mpl = state.sensors[DATA_TYPES::temp_int_mpl].value;
        msg.temp_int_xsens = state.sensors[DATA_TYPES::temp_int_xsens].value;

        msg.temp_ext = state.sensors[DATA_TYPES::temp_ext].value;
        msg.press_int = state.sensors[DATA_TYPES::press_int].value;
        msg.press_ext = state.sensors[DATA_TYPES::press_ext].value;
        msg.humidity = state.sensors[DATA_TYPES::humidity].value;
        msg.current_3V = state.sensors[DATA_TYPES::current_3V].value;
        msg.current_5V = state.sensors[DATA_TYPES::current_5V].value;
        msg.current_bat = state.sensors[DATA_TYPES::current_bat].value;
	msg.current_12Vcam = state.sensors[DATA_TYPES::cam_current].value;
	msg.current_12Valt = state.sensors[DATA_TYPES::altimeter_current].value;

        msg.rbat_voltage = state.sensors[DATA_TYPES::rbat_voltage].value;
        msg.r5V_voltage = state.sensors[DATA_TYPES::r5V_voltage].value;
        msg.r3V_voltage = state.sensors[DATA_TYPES::r3V_voltage].value;
	msg.r12VCam_voltage =  state.sensors[DATA_TYPES::cam_voltage].value;
	msg.r12VAlt_voltage = state.sensors[DATA_TYPES::altimeter_voltage].value;


        msg.latitude = state.sensors[DATA_TYPES::latitude].value;
        msg.longitude = state.sensors[DATA_TYPES::longitude].value;
        msg.status = (int) state.sensors[DATA_TYPES::status].value;
        msg.pwm_ul = state.sensors[DATA_TYPES::pwm_ul].value;
        msg.pwm_ur = state.sensors[DATA_TYPES::pwm_ur].value;
        msg.pwm_fr = state.sensors[DATA_TYPES::pwm_fr].value;
        msg.pwm_fl = state.sensors[DATA_TYPES::pwm_fl].value;
        

        float r = state.sensors[DATA_TYPES::roll].value, 
               p = state.sensors[DATA_TYPES::pitch].value,
               y =  state.sensors[DATA_TYPES::yaw].value;

        tf2::Matrix3x3 rot , cord_tranf, transformed; 
        rot.setRPY(deg2rad(r),deg2rad(p),deg2rad(y)); 
        cord_tranf.setRPY(0,0,-3.14/2);
        transformed = rot * cord_tranf; 


        double r2, p2, y2;
        transformed.getRPY(r2,p2,y2);

        msg.roll = rad2deg(r2);
        msg.pitch = rad2deg(p2);
        msg.yaw = rad2deg(y2);

	
	tf2::Vector3 gravity = {0.0, 0.0, -9.81};
	
	tf2::Vector3 gravity_accel = rot * gravity;
	
        msg.accel_x = state.sensors[DATA_TYPES::accel_x].value; // - gravity_accel[0]; 
        msg.accel_y = state.sensors[DATA_TYPES::accel_y].value;  // - gravity_accel[1];
        msg.accel_z = state.sensors[DATA_TYPES::accel_z].value; // - gravity_accel[2];

	msg.gyro_x = state.sensors[DATA_TYPES::gyro_x].value;
	msg.gyro_y = state.sensors[DATA_TYPES::gyro_y].value;
	msg.gyro_z = state.sensors[DATA_TYPES::gyro_z].value;
	

        msg.user_iface = (int) state.sensors[DATA_TYPES::user_iface].value;

        ll_data_pub.publish(msg);
    }

	void publish_depth() {
		// Convert the pressure into a depth measurement and publish it
		utils::PressureSensor msg;

		msg.header.stamp = ros::Time::now();
		msg.header.frame_id = "/externalPressure_frame";
		msg.pressure = state.sensors[DATA_TYPES::press_ext].value; // In Pa
		msg.depth = (state.sensors[DATA_TYPES::press_ext].value - atmosphericPressure)/(1025*9.81); //In meters

		depth_pub.publish(msg);
        pressure_freq->tick();
	}

	void publish_altitude() {
        utils::Altitude msg;

        msg.header.stamp = ros::Time::now();
    		msg.header.frame_id = "/externalPressure_frame";
    		msg.altitude = state.sensors[DATA_TYPES::altitude].value; // In Pa

        altimeter_pub.publish(msg);
        altimeter_freq->tick();
	}

    void publish_gps() {
        sensor_msgs::NavSatFix msg;
        msg.header.stamp = ros::Time::now();
        msg.header.frame_id = veh_name+"/gps_frame";
        msg.latitude = NED::degreeMinute2Degree(state.sensors[DATA_TYPES::latitude].value);  // Convert from DDDMM.MM to DDD.DD format used for GPS messages
        msg.longitude = NED::degreeMinute2Degree(state.sensors[DATA_TYPES::longitude].value);
        //std::cout <<std::fixed<< "Latitude: "<<state.sensors[DATA_TYPES::latitude].value<<" Longitude: "<<state.sensors[DATA_TYPES::longitude].value<<std::endl;
        //msg.status = 0;
        if (msg.latitude!=0 && msg.longitude!=0) {
          gps_pub.publish(msg);
          gps_freq->tick();
        }
  }

	void publish_imu() {
		sensor_msgs::Imu msg; 

        // Convert un
        double timestamp = state.sensors[DATA_TYPES::imu_timestamp].value;


		msg.header.stamp = ros::Time::now();
		msg.header.frame_id = "/imu_frame";
		tf2::Quaternion quat;
        quat.setRPY(deg2rad(state.sensors[DATA_TYPES::roll].value),
                    deg2rad(state.sensors[DATA_TYPES::pitch].value), 
                    deg2rad(state.sensors[DATA_TYPES::yaw].value));
		msg.orientation.x = quat.x();
		msg.orientation.y = quat.y();
		msg.orientation.z = quat.z();
		msg.orientation.w = quat.w();
		
	 	float r = state.sensors[DATA_TYPES::roll].value,
                p = state.sensors[DATA_TYPES::pitch].value,
                y =  state.sensors[DATA_TYPES::yaw].value;

        tf2::Matrix3x3 rot , cord_tranf, transformed;
        rot.setRPY(deg2rad(r),deg2rad(p),deg2rad(y));
		tf2::Vector3 gravity = {0.0, 0.0, -9.81};
		tf2::Vector3 gravity_accel = rot * gravity;      
        
        msg.angular_velocity.x = state.sensors[DATA_TYPES::gyro_x].value; 
        msg.angular_velocity.y = state.sensors[DATA_TYPES::gyro_y].value; 
        msg.angular_velocity.z = state.sensors[DATA_TYPES::gyro_z].value; 

		msg.linear_acceleration.x = state.sensors[DATA_TYPES::accel_x].value; // - gravity_accel[0];
        msg.linear_acceleration.y = state.sensors[DATA_TYPES::accel_y].value; // - gravity_accel[1];
        msg.linear_acceleration.z = state.sensors[DATA_TYPES::accel_z].value; // - gravity_accel[2];
		imu_pub.publish(msg);
        imu_freq->tick();
	}

    template <class T>
    T deg2rad(T& val) {
        return (val * M_PI) / 180;
    }

    template <class T>
    T rad2deg(T& radians) {
        return (radians * 180) / M_PI ;
    }

    uint32_t onSS(uint32_t myarg) {
        printf("Got SS: %f \n\r", state.sensors[DATA_TYPES::status].value);
        return 0;
    }

    uint32_t onUI(uint32_t myarg) {
        printf("Got UI: %f \n\r", state.sensors[DATA_TYPES::user_iface].value);
        return 0;
    }


    void thruster_callback(const utils::ThrustersData::ConstPtr &msg) {
        //ROS_ERROR_STREAM("I heard: "<< msg->ul);
	    ROS_DEBUG("Setting thruster data");
        // Thruster setpoint go from 0 for full reverse to 1 for full forward. 0.5 is neutral. 
        int id_array[4] = {DATA_TYPES::pwm_ul, DATA_TYPES::pwm_ur, DATA_TYPES::pwm_fl, DATA_TYPES::pwm_fr};
        double th_array[4] = {msg->ul, msg->ur, msg->fl, msg->fr};
        int length;
        std::string message(protocol.createmsg(SerialProtocol::ST, id_array, th_array, 4, &length ));
        ser.write(message);
    }

/* Get current time and send it to the LPC1768 to synchronize the clocks 
*/
    void synchronize_time() {
        int id[1] = {DATA_TYPES::current_time}; 
        ros::Time timestamp = ros::Time::now();
        double secs = timestamp.toSec();
        ROS_INFO("Sending time %f",secs);
        ROS_INFO("ROS Timestamp sec field %i and ns %i", timestamp.sec, timestamp.nsec);
        int length; 
        double datarray[1] = {timestamp.sec};
        std::string message(protocol.createmsg(SerialProtocol::DM, id, datarray,1, &length));
        ser.write(message);
    }
};



/**
 * Node entry-point. Handles ROS setup, and serial port connection/reconnection.
 */
int main(int argc, char **argv)
{
    ros::init(argc, argv, "sphere_hw_iface");
    ros::NodeHandle nh("~");
    ROS_INFO("Sphere name: %s",argv[1]);
    SphereAdapter sp_hw_iface(nh,argv[1]);
    sp_hw_iface.connect();
    
}


