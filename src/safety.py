#!/usr/bin/env python

from __future__ import division

import numpy as np

from utils.msg import LowLevelData


import rospy
import roslib

class SafetyWatch():
    def __init__(self, name):

        self.name = name

        self.sub_data = rospy.Subscriber('/sphere_hw_iface/ll_data', LowLevelData, self.save,queue_size=1)

        self.ll_data = LowLevelData()

        rospy.Timer(rospy.Duration(1), self.watch)

    def save(self, msg):
        self.ll_data = msg

    def watch(self, dummy):

        msg = self.ll_data
        # Check power voltages
        if (msg.rbat_voltage<6.5) and (msg.rbat_voltage>6):
            rospy.logwarn("Low Battery")
        if msg.rbat_voltage<=6:
            rospy.logerr("Very Low Battery: Stop immediately to avoid damage to battery cells")

        if msg.r5V_voltage <2.5:
            rospy.logwarn("No power on 5V rail")
        if msg.r5V_voltage <4.7:
            rospy.logwarn("Low voltage on 5V rail")
        if msg.r3V_voltage <1.5:
            rospy.logwarn("No power on 3V rail")
        if msg.r3V_voltage <3:
            rospy.logwarn("Low voltage on 3V rail")

        # Check internal temperatures
        #if (msg.temp_int_hih >= 50) and (msg.temp_int_hih < 60):
        #    rospy.logwarn("HIH Sensor: Temperatures over 50deg Celsius")
        #if (msg.temp_int_hih >= 60) and (msg.temp_int_hih < 70):
        #    rospy.logwarn("HIH Sensor: Temperatures over 60deg Celsius")
        #if (msg.temp_int_hih >= 70) and (msg.temp_int_hih < 80):
        #    rospy.logwarn("HIH Sensor: Temperatures over 70deg Celsius")
        #if msg.temp_int_hih >= 80:
        #    rospy.logerr("HIH Sensor: Temperatures over 80deg Celsius. Risk of overheating")

        if (msg.temp_int_mpl >= 50) and (msg.temp_int_mpl < 60):
            rospy.logwarn("MPL Sensor: Temperatures over 50deg Celsius")
        if (msg.temp_int_mpl >= 60) and (msg.temp_int_mpl < 70):
            rospy.logwarn("MPL Sensor: Temperatures over 60deg Celsius")
        if (msg.temp_int_mpl >= 70) and (msg.temp_int_mpl < 80):
            rospy.logwarn("MPL Sensor: Temperatures over 70deg Celsius")
        if msg.temp_int_mpl >= 80:
            rospy.logerr("MPL Sensor: Temperatures over 80deg Celsius. Risk of overheating")

        if (msg.press_int >=55000) and (msg.press_int< 60000 ): 
            rospy.logwarn("Internal Pressure between 55kPa and 60kPa")
        if (msg.press_int >=60000) and (msg.press_int< 65000 ): 
            rospy.logwarn("Internal Pressure between 60kPa and 65kPa")
        if (msg.press_int >=65000) and (msg.press_int< 70000 ): 
            rospy.logwarn("Internal Pressure between 65kPa and 70kPa")
        if (msg.press_int >=70000): 
            rospy.logerr("Very low vacuum: Retrieve vehicle immediately")


def main():
    rospy.init_node('Safety_Watch')
    name = rospy.get_name()
    rospy.loginfo('%s initializing ...', name)

    watch = SafetyWatch(name)
    rospy.spin()

if __name__ == '__main__':
    main()