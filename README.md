This repository contains code developed for the DROP-Sphere. More information about the project can be found [here](http://www.dropsphere.net)

# Sphere Hardware Adapter 

This ROS node is responsible for communicating with the LPC1768 microcontroller using a serial port and acts as an interface between the low-level sensor drivers and ROS. 

